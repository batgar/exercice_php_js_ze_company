<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\LivreRepository;
use App\Repository\AuteurRepository;
use App\Form\Type\LivreType;
use App\Entity\Livre;
use App\Entity\Auteur;

class LivreController extends AbstractController
{
    /**
     * @Route("/livre", name="livre")
     */
    public function index(LivreRepository $livreRepository)
    {
        return $this->render('livre/index.html.twig', [
            'livres' => $livreRepository->findAll()]);
        
    }

    /**
     * @Route("/livre/new", name="livre/new", methods="GET|POST")
     */
    public function new(Request $request)
    {
        $livre = new Livre();

        $repository = $this->getDoctrine()->getRepository(Livre::class);
        
        $form = $this->createForm(LivreType::class, $livre);
        $form->handleRequest($request);
        
        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            
            $livre->setTitre($request->request->get('livre')["titre"]);
            $livre->setPrix($request->request->get('livre')["prix"]);
            
            $auteurId = $request->request->get('livre')["auteur"];;
            
            $auteur = $this->getDoctrine()->getRepository(Auteur::class)->find($auteurId);

            $livre->setAuteur($auteur);

            $em->persist($livre);
            $em->flush();

            return $this->redirectToRoute('livre');
        }

        return $this->render('livre/new.html.twig', [
            'livre' => $livre,
            'form' => $form->createView(),
        ]);
    }
}
