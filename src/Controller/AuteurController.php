<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\AuteurRepository;
use App\Form\Type\AuteurType;
use App\Entity\Auteur;

class AuteurController extends AbstractController
{
    /**
     * @Route("/auteur", name="auteur")
     */
    public function index(AuteurRepository $auteurRepository)
    {
        return $this->render('auteur/index.html.twig', [
            'auteurs' => $auteurRepository->findAll()]);
        
    }

    /**
     * @Route("/auteur/new", name="auteur/new", methods="GET|POST")
     */
    public function new(Request $request)
    {
        // creates a personne object and initializes some data
        $auteur = new Auteur();

        $repository = $this->getDoctrine()->getRepository(Auteur::class);

        $form = $this->createForm(AuteurType::class, $auteur);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $auteur->setNom($request->request->get('auteur')["nom"]);
            $auteur->setPrenom($request->request->get('auteur')["prenom"]);

            $em->persist($auteur);
            $em->flush();

            return $this->redirectToRoute('auteur');
        }

        return $this->render('auteur/new.html.twig', [
            'auteur' => $auteur,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/auteur/{id}", name="auteur_show", methods="GET")
     */
    public function show(Auteur $auteur)
    {
        return $this->render('auteur/show.html.twig', [
            'auteur' => $auteur,
        ]);
    }
}
