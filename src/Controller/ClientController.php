<?php

namespace App\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\HttpFoundation\Request;
use App\Repository\ClientRepository;
use App\Form\Type\ClientType;
use App\Entity\Client;

class ClientController extends AbstractController
{
    /**
     * @Route("/client", name="client")
     */
    public function index(ClientRepository $clientRepository)
    {
        // dump($clientRepository->findAll()); die();
        return $this->render('client/index.html.twig', [
            'clients' => $clientRepository->findAll(),
        ]);
    }

    /**
     * @Route("/client/new", name="client/new", methods="GET|POST")
     */
    public function new(Request $request)
    {
        $client = new Client();

        $repository = $this->getDoctrine()->getRepository(Client::class);

        $form = $this->createForm(ClientType::class, $client);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {
            $em = $this->getDoctrine()->getManager();

            $client->setNom($request->request->get('client')["nom"]);
            $client->setPrenom($request->request->get('client')["prenom"]);
            
            $date = $request->request->get('client')["dateDeNaissance"];
            $dateNaiss = new \DateTime(strval($date["year"]) . "-" . strval($date["month"]) . "-" . strval($date["day"]));
            $client->setDateDeNaissance($dateNaiss);

            $em->persist($client);
            $em->flush();

            return $this->redirectToRoute('client');
        }

        return $this->render('client/new.html.twig', [
            'client' => $client,
            'form' => $form->createView(),
        ]);
    }

    /**
     * @Route("/client/{id}", name="client_show", methods="GET")
     */
    public function show(Client $client)
    {
        return $this->render('client/show.html.twig', [
            'client' => $client,
        ]);
    }
}
